import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import BillService from '@/services/bill'
import type { Bill } from '@/types/Bill'

export const useBillStore = defineStore('Bill', () => {
    const loadingStore = useLoadingStore()
    const Bills = ref<Bill[]>([])

    async function getBill(id: number) {
        loadingStore.doLoad()
        const res = await BillService.getBill(id)
        Bills.value = res.data
        loadingStore.finish()
    }
    async function getBills() {
        loadingStore.doLoad()
        const res = await BillService.getBills()
        Bills.value = res.data
        loadingStore.finish()
    }
    async function saveBill(Bill: Bill) {
        loadingStore.doLoad()
        if (Bill.id < 0) {
            //Add new
            console.log('Post ' + JSON.stringify(Bill))
            const res = await BillService.addBill(Bill)
        } else {
            //Update
            console.log('Patch ' + JSON.stringify(Bill))
            const res = await BillService.updateBill(Bill)
        }
        await getBills()
        loadingStore.finish()
    }
    async function deleteBill(Bill: Bill) {
        loadingStore.doLoad()
        const res = await BillService.delBill(Bill)
        await getBills()
        loadingStore.finish()
    }
    return { Bills, getBills, saveBill, deleteBill }
})