import type { Bill } from "@/types/Bill";
import http from "./http";

function addBill(Bill: Bill) {
    return http.post('/Bills', Bill)
}

function updateBill(Bill: Bill) {
    return http.patch(`/Bills/${Bill.id}`, Bill)
}

function delBill(Bill: Bill) {
    return http.delete(`/Bills/${Bill.id}`)
}

function getBill(id: number) {
    return http.delete(`/Bills/${id}`)
}

function getBills() {
    return http.get('/Bills')
}

export default { addBill, updateBill, delBill, getBill, getBills }