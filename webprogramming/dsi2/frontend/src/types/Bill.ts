type Bill = {
    id: number //? บอกว่า id เป็นอ optional คือ ตัวแปรของชนิดข้อมูล bill อาจมีหรือไม่มีคุณสมบัตินี้ก็ได้
    name: string //ชื่อพนักงาน
    salary: number //เงินเดือนพนักงาน
    position: string //ตำแหน่งงาน
    workingtime: number //เวลาทำงาน
    status: string // สถานะการจ่ายเงิน
}
export { type Bill }